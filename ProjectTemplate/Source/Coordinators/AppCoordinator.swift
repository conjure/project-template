//
//  AppCoordinator.swift
//  Conjure
//
//  Created by Piotr Wilk on 04/08/2021.
//

import UIKit

class AppCoordinator: Coordinator {
    var parent: Coordinator?
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    required init(_ navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let viewController = ViewController()
        viewController.view.backgroundColor = .orange
        navigationController.pushViewController(viewController, animated: false)
    }
}
