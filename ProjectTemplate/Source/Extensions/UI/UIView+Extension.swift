//
//  UIView+Extension.swift
//  Conjure
//
//  Created by Piotr Wilk on 04/08/2021.
//

import UIKit

extension UIView {
    func asImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
    
    func addToFill(_ view: UIView, insets: UIEdgeInsets = .zero) {
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        
        let constraints = [view.leadingAnchor.constraint(equalTo: leadingAnchor, constant: insets.left),
                           view.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -insets.right),
                           view.topAnchor.constraint(equalTo: topAnchor, constant: insets.top),
                           view.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -insets.bottom)]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.2) {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        animation.toValue = toValue
        animation.duration = duration
        animation.isRemovedOnCompletion = false
        animation.fillMode = CAMediaTimingFillMode.forwards
        self.layer.add(animation, forKey: nil)
    }
}
