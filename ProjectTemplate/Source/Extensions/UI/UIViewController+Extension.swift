//
//  UIViewController+Extension.swift
//  Conjure
//
//  Created by Piotr Wilk on 04/08/2021.
//

import UIKit

extension UIViewController {
    func add(child: UIViewController, to: UIView? = nil, animated: Bool = false) {
        addChild(child)
        
        let parentView = to ?? view
        parentView?.addToFill(child.view)
        child.didMove(toParent: self)
        if animated {
            child.view.alpha = 0
            UIView.animate(withDuration: 0.2) {
                child.view.alpha = 1
            }
        }
    }
    
    func remove() {
        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
    }
    
    func dismissAllPresentedModals(_ animated: Bool = false) {
        var viewControllers = [UIViewController]()
        var currentViewController = self
        
        while let presentedVC = currentViewController.presentedViewController {
            viewControllers.append(currentViewController)
            currentViewController = presentedVC
        }
        viewControllers.forEach { $0.dismiss(animated: animated, completion: nil) }
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
