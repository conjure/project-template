//
//  UIStoryboard+Extension.swift
//  Conjure
//
//  Created by Piotr Wilk on 04/08/2021.
//

import UIKit

extension UIStoryboard {
    public class func instantiateViewController<T: UIViewController>() -> T {
        let storyboard = UIStoryboard(name: T.className, bundle: .main)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: T.className) as? T else {
            fatalError("Could not instantiate initial storyboard with name: \(T.className)")
        }
        
        return viewController
    }
}
