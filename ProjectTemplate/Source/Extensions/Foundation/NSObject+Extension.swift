//
//  NSObject+Extension.swift
//  Conjure
//
//  Created by Piotr Wilk on 04/08/2021.
//

import Foundation

extension NSObject {
    class var className: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
    
    var className: String {
        let thisType = type(of: self)
        return String(describing: thisType)
    }
}
