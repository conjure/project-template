//
//  String+Extension.swift
//  Conjure
//
//  Created by Piotr Wilk on 04/08/2021.
//

import Foundation

extension String {
    func capitalizingFirstLetter() -> String {
      return prefix(1).uppercased() + self.lowercased().dropFirst()
    }

    mutating func capitalizeFirstLetter() {
      self = self.capitalizingFirstLetter()
    }
    
    var trimmed: String {
        return trimmingCharacters(in: .whitespaces)
    }
    
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
