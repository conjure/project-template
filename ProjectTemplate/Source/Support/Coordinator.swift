//
//  Coordinator.swift
//  Conjure
//
//  Created by Piotr Wilk on 04/08/2021.
//

import UIKit

protocol Coordinator: AnyObject {
    var childCoordinators: [Coordinator] { get set }
    var parent: Coordinator? { get set }
    init(_ navigationController: UINavigationController)
    var navigationController: UINavigationController { get set }
    func start()
    func add(child: Coordinator)
    func removeFromParent()
}

extension Coordinator {
    public func childDidFinish(_ child: Coordinator?) {
        for (index, coordinator) in childCoordinators.enumerated() where coordinator === child {
            childCoordinators.remove(at: index)
        }
    }
    
    public func add(child: Coordinator) {
        child.parent = self
        childCoordinators.append(child)
    }
    
    public func removeFromParent() {
        parent?.childDidFinish(self)
    }
}
