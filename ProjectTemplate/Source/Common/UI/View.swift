//
//  View.swift
//  Conjure
//
//  Created by Piotr Wilk on 04/08/2021.
//

import UIKit

public class View: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setUp()
    }
    
    func setUp() {
        setUpSubviews()
        setUpLayout()
    }
    
    func setUpSubviews() {}
    
    func setUpLayout() {}
}
