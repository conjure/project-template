//
//  ViewController.swift
//  Conjure
//
//  Created by Piotr Wilk on 04/08/2021.
//

import UIKit

public class ViewController: UIViewController {
    
    public override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
